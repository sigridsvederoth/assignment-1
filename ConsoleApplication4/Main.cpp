// Main.cpp

#include "stdafx.h"
#include <iostream>
#include "LinkedList.h"
#include "BinaryTree.h"
#include "UnitTesting.h"

int _tmain(int argc, _TCHAR* argv[])
{	
	

	//
	//--------------------------------------------------------------LINKEDLIST--------------------------------------------------------------
	//

	std::string line;
	int number;

	LinkedList<int> *list = new LinkedList<int>;
	std::cout << "LinkedList" << std::endl;
	list->pushback(5);
	list->pushback(7);
	list->pushback(8);
	list->pushback(9);
	list->pushback(10);
	list->pushfront(2);
	verify<int>(true, list->find(10), "Pushback()");
	verify<int>(true, list->find(2), "Pushfront()");
	verify<int>(6, list->size(), "Size()");
	list->pop_front();
	verify<int>(5, list->size(), "Popfront()");
	list->pop_back();
	verify<int>(4, list->size(), "Popback()");
	list->clear();
	verify<int>(0, list->size(), "Clear()");
	
	//
	//--------------------------------------------------------------BINARYSEARCHTREE--------------------------------------------------------------
	//

	BinaryTree<int> *BinarySearchTree = new BinaryTree < int >;
	std::cout << std::endl << "BinarySearchTree" << std::endl;
	BinarySearchTree->Insert(5);
	BinarySearchTree->Insert(8);
	BinarySearchTree->Insert(1);
	BinarySearchTree->Insert(6);
	BinarySearchTree->Insert(9);
	BinarySearchTree->Insert(14);
	verify<int>(6, BinarySearchTree->size(), "Insert()");
	verify<int>(true, BinarySearchTree->find(9), "Find()");
	std::cout << "Traversal In Order: ";
	BinarySearchTree->traversal_inOrder();
	std::cout << "Traversal Post Order: ";
	BinarySearchTree->traversal_postOrder();
	std::cout << "Traversal Pre Order: ";
	BinarySearchTree->traversal_preOrder();
	verify<int>(6, BinarySearchTree->size(), "Size()");
	BinarySearchTree->clear();
	verify<int>(0, BinarySearchTree->size(), "Clear()");

	std::string party;
	std::cin >> party;

	return 0;
}
