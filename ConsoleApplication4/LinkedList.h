//LinkedList.h
#include "stdafx.h"

template <class T>
class LinkedList
{
public:
	LinkedList() : root(nullptr)
	{

	}

	~LinkedList()
	{

	}
	
	//
	//--------------------------------------------------------------PUSHBACK--------------------------------------------------------------
	//
	void pushback(T x)
	{
		Node<T> *node = new Node<T>;
		node->value = x;
		node->next = nullptr;

		if (root == nullptr)
		{
			root = node;
		}
		else
		{
			Node<T>* tempNode = nullptr;
			tempNode = root;
			while (tempNode->next != nullptr)
			{
				tempNode = tempNode->next;
			}

			tempNode->next = node;
		}
	}
	
	//
	//--------------------------------------------------------------PUSHFRONT--------------------------------------------------------------
	//
	void pushfront(T x)
	{
		Node<T>* node = new Node<T>;
		node->value = x;

		node->next = root;
		root = node;
	}
	
	//
	//--------------------------------------------------------------POP_FRONT--------------------------------------------------------------
	//
	void pop_front()
	{
		Node<T>* popNode = root;
		root = popNode->next;
		delete popNode;
	}
	
	//
	//--------------------------------------------------------------POP_BACK--------------------------------------------------------------
	//
	void pop_back()
	{
		Node<T>* popNode = root;
		
		while (popNode->next->next)
		{
			popNode = popNode->next;
		}
		delete popNode->next;
		popNode->next = nullptr;
	}
	
	//
	//--------------------------------------------------------------SIZE--------------------------------------------------------------
	//
	int size()
	{
		int count = 0;
		Node<T>* node = root;

		while (node)
		{
			count++;
			node = node->next;
		}
		return count;
	}
	
	//
	//--------------------------------------------------------------CLEAR--------------------------------------------------------------
	//
	void clear()
	{
		Node<T>* node = root;

		while (node != nullptr)
		{
			Node<T>* noode = node->next;
			delete node;
			node = noode;
		}

		root = nullptr;
	}

	//
	//--------------------------------------------------------------FIND--------------------------------------------------------------
	//
	bool find(T x)										//bool returnerar falskt eller sant, vi letar efter int x som �r ett v�rde vi skriver in i main.cpp
	{
		Node<T>* tempnode;
		tempnode = root;

		while (tempnode != nullptr)						//s� l�nge det finns en node med v�rde s� g�r vi igenom
		{
			if (x == tempnode->value)					//om v�rdet vi letar efter �r samma som nodens v�rde
			{
				return true;							//om det st�mmer returneras true, dvs 1
			}
			else										//om det inte st�mmer s� forts�tter vi kolla igenom noderna
			{
				tempnode = tempnode->next;
			}
		}
		return false;									//om int x inte hittas alls och alla noder �r slut returneras 0 som �r falskt

	}

private:
	template <typename T>
	struct Node
	{
		T value;
		Node<T> *next;

	public:
		Node() : next(nullptr)
		{}
	};

	Node<T> *root; //f�rsta delen av linkedlist
};


