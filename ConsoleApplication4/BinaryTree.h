//BinaryTree.h

#include "stdafx.h"

template <typename T>
struct Node
{
	Node<T>* Bigger = nullptr;
	Node<T>* Smaller = nullptr;
	T value;
};

template <class T>
class BinaryTree
{
public:
	BinaryTree()
	{
		root = nullptr;
	}

	~BinaryTree()
	{

	}
	
	//
	//--------------------------------------------------------------INSERT--------------------------------------------------------------
	//
	void Insert(T x)
	{
		Node<T> *currentNode = root;

		if (root == nullptr){
			root = new Node<T>;
			root->value = x;
			return;
		}

		while (currentNode != nullptr)
		{
			if (x == currentNode->value)
				return;
			else if (x > currentNode->value)
			{
				if (currentNode->Bigger == nullptr)
				{
					currentNode->Bigger = new Node<T>;
					currentNode->Bigger->value = x;
				}
				else
				{
					currentNode = currentNode->Bigger;
				}
			}
			else
			{
				if (currentNode->Smaller == nullptr)
				{
					currentNode->Smaller = new Node<T>;
					currentNode->Smaller->value = x;
				}
				else
				{
					currentNode = currentNode->Smaller;
				}
			}
		}
	}
	
	//
	//--------------------------------------------------------------SIZE--------------------------------------------------------------
	//
	int size()
	{
		int size = 0;
		sizeoftree(root, size);
		return size;
	}

	void sizeoftree(Node<T>* node, int &size)
	{
		if (node != nullptr)
		{
			size++;
			sizeoftree(node->Bigger, size);
			sizeoftree(node->Smaller, size);
		}
	}
	
	//
	//--------------------------------------------------------------CLEAR--------------------------------------------------------------
	//
	void clear(Node<T>* node)
	{
		if (node == nullptr)
			return;

		clear(node->Bigger);
		clear(node->Smaller);
		node = nullptr;
		delete node;
	}
	
	void clear()
	{
		clear(root);
		root = nullptr;
	}
	
	//
	//--------------------------------------------------------------FIND--------------------------------------------------------------
	//
	bool find(T x)
	{
		Node<T>* currentnode;
		currentnode = root;
		
		if (root == nullptr)
			return false;

		else if (findnode(x, currentnode))
			return true;

		else
			return false;
		
	}

	bool findnode(T x, Node<T>* node)
	{
		
		if (x == node->value)
			return true;

		else if (x < node->value)
		{
			if (node->Smaller == nullptr)
				return false;

			else
			{
				node = node->Smaller;
				findnode(x, node);
			}
		}
		
		else if (x > node->value)
		{
			if (node->Bigger == nullptr)
				return false;

			else
			{
				node = node->Bigger;
				findnode(x, node);
			}
		}
	}
	//
	//--------------------------------------------------------------PRE-ORDER--------------------------------------------------------------
	//

	void traversal_preOrder()
	{
		Node<T>* currentNode = root;
		if (root)
			traversal_preOrder(root);
		std::cout << std::endl;
	}

	void traversal_preOrder(Node<T>* currentNode)
	{
		std::cout << currentNode->value << " ";

		if (currentNode->Smaller)
			traversal_preOrder(currentNode->Smaller);
		if (currentNode->Bigger)
			traversal_preOrder(currentNode->Bigger);
	}

	//
	//--------------------------------------------------------------IN-ORDER--------------------------------------------------------------
	//

	void traversal_inOrder()
	{
		Node<T>* currentNode = root;
		if (root)
			traversal_inOrder(currentNode);
		std::cout << std::endl;
	}

	void traversal_inOrder(Node<T>* currentNode)
	{
		if (currentNode->Smaller)
			traversal_inOrder(currentNode->Smaller);
		std::cout << currentNode->value << " ";
		if (currentNode->Bigger)
			traversal_inOrder(currentNode->Bigger);
	}

	//
	//--------------------------------------------------------------POST-ORDER--------------------------------------------------------------
	//

	void traversal_postOrder()
	{
		Node<T>* currentNode = root;
		if (root)
			traversal_postOrder(currentNode);
		std::cout << std::endl;
	}

	void traversal_postOrder(Node<T>* currentNode)
	{
		if (currentNode->Smaller)
			traversal_postOrder(currentNode->Smaller);
		if (currentNode->Bigger)
			traversal_postOrder(currentNode->Bigger);
		std::cout << currentNode->value << " ";
	}
private:

	Node<T>* root;
};